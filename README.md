## Gitlab introduction course MS-ICD
#### kevin.balem@ifremer.fr

## Mercredi 21/09 : 13h45-18h
Partie théorique (voir pdf)

## Vendredi 23/09 : 09h30-12h15
### Travaux dirigés, mini-projets

1. Consituter vos équipes et choisissez un projet simple de développement (python, bash, ...)  
_**Exemples de l'an passé** : générateur de mot de passe, simulateur de lancé de dés, convertisseur de devises_
2. Réunissez vous et l'un des membres ("leader de projet") va : 
    - créer le projet gitlab
    - inviter les autres membres dans le projet (+ @k17balem en tant que developpeur)
    - créer le readme
    - créer un premier fichier de code
3. Créer une issue pour chaque fonctionnalités  
4. Chaque développeur va ensuite sur son poste et démarre une branche pour ajouter une fonctionnalité
5. Chaque développeur va créé une "Merge request" correspondant à sa branche
6. Une fois le développement terminé, utiliser la Merge resquest pour merger la branche


## Exemple

---

## TEMPLATE DU RAPPORT DE TD

Noms et logins gitlab-imt des membres du projet :  
Date :   
Nom du projet :  
URL Gitlab du projet :  

## 1. Objectifs et description du projet 

Décrire les objectifs du projet (en terme d’apprentissage et de fonctionnalités), et décrire simplement les fonctionnalités.

## 2. Description du workflow

Décrire les différentes étapes de construction de votre projet en faisant apparaître les termes vus en cours (add, clone, push, pull, merge, merge request, issue, gitlab, repo, remote, …)
